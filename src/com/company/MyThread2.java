package com.company;

public class MyThread2 extends Thread {
    public MyThread1 myThread1;

    public synchronized void printThreadName(){
        System.out.println(myThread1.getThreadName());
    }

    public synchronized String getThreadName(){
        return this.getName();
    }

    @Override
    public void run() {
        myThread1.printThreadName();
    }
}

