package com.company;

public class MyThread1 extends Thread {
    public MyThread2 myThread2;

    public synchronized void printThreadName(){
        System.out.println(myThread2.getThreadName());
    }

    public synchronized String getThreadName(){
        return this.getName();
    }

    @Override
    public void run() {
        myThread2.printThreadName();
    }
}
