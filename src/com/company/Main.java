package com.company;

public class Main {

    public static void main(String[] args) {
        MyThread2 myThread2 = new MyThread2();
        MyThread1 myThread1 = new MyThread1();
        myThread2.myThread1 = myThread1;
        myThread1.myThread2 = myThread2;
        myThread1.start();
        myThread2.start();
    }
}
